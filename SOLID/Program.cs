﻿using Microsoft.Extensions.DependencyInjection;
using SOLID.GuessNumberGame;
using SOLID.Helpers;
using SOLID.Interfaces;

namespace SOLID;

static class Program
{
    static void Main(string[] args)
    {
        var provider = Configure();
        var menu = provider.GetService<IMenu>();
        menu.Start();
    }

    private static IServiceProvider Configure()
    {
        var services = new ServiceCollection();

        services.AddSingleton<IConsoleInputDataHelper, ConsoleHelper>();
        services.AddSingleton<IGuessNumberGameSettings, SettingsGuessNumberGame>();
        services.AddSingleton<IMenu, MenuGuessNumberGame>();
        services.AddTransient<IGame, GameGuessNumberGame>();

        return services.BuildServiceProvider();
    }
}