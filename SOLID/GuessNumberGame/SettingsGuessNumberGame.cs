﻿using SOLID.Interfaces;

namespace SOLID.GuessNumberGame;

public class SettingsGuessNumberGame : RandomSettingsGuessNumberGame
{
    private readonly IConsoleInputDataHelper _consoleHelper;

    public SettingsGuessNumberGame(IConsoleInputDataHelper consoleHelper) : base()
    {
        _consoleHelper = consoleHelper;
    }

    public override void ChangeSettings()
    {
        Console.WriteLine("Изменение настроек");

        Console.WriteLine("Диапазон для загадывания числа от");
        From = _consoleHelper.GetIntFromConsole();

        Console.WriteLine("Диапазон для загадывания числа до");
        var toChanged = false;
        do
        {
            var to = _consoleHelper.GetIntFromConsole();
            if (to < From)
            {
                Console.WriteLine("Значение не может быть меньше чем значение \"от\"");
                continue;
            }
            To = to;
            toChanged = true;
        } while (!toChanged);

        Console.WriteLine("Количество попыток");
        Attempts = _consoleHelper.GetUintFromConsole();
    }
}