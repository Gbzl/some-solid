﻿using Microsoft.Extensions.DependencyInjection;
using SOLID.Interfaces;

namespace SOLID.GuessNumberGame;

public class MenuGuessNumberGame : IMenu
{
    private readonly IGuessNumberGameSettings _settings;
    private readonly IServiceProvider _serviceProvider;

    public MenuGuessNumberGame(IGuessNumberGameSettings settings, IServiceProvider serviceProvider)
    {
        _settings = settings;
        _serviceProvider = serviceProvider;
    }

    public void Start()
    {
        var leaveGame = false;

        WelcomeSpeech();

        do
        {
            var choice = Console.ReadLine();
            if (choice == null || !int.TryParse(choice, out var result))
            {
                Console.WriteLine("Введеное значение не является числом, попробуйте еще раз");
                continue;
            }

            switch (result)
            {
                case 1:
                    Console.Clear();
                    var game = _serviceProvider.GetService<IGame>();
                    game.Play();
                    Console.Clear();
                    WelcomeSpeech();
                    break;
                case 2:
                    Console.Clear();
                    _settings.ChangeSettings();
                    Console.Clear();
                    WelcomeSpeech();
                    break;
                case 0:
                    Console.WriteLine("Пока");
                    leaveGame = true;
                    break;
                default:
                    Console.Clear();
                    WelcomeSpeech();
                    Console.WriteLine("Для заданного числа нет команды, попробуйте еще раз");
                    break;
            }
        } while (!leaveGame);
    }

    private void WelcomeSpeech()
    {
        Console.WriteLine("Добро пожаловать в игру \"Угадай число\"\n" +
                          "Для старта игры нажмите 1\n" +
                          $"Текущие найстройки игры: диапазон от {_settings.From} до {_settings.To}, количество попыток {_settings.Attempts}\n" +
                          "Для изменения настроек нажмите 2\n" +
                          "Для выхода из игры нажмите 0");
    }
}