﻿using SOLID.Interfaces;

namespace SOLID.GuessNumberGame;

public class GameGuessNumberGame : IGame
{
    private readonly IGuessNumberGameSettings _settings;
    private readonly IConsoleInputDataHelper _consoleHelper;

    public GameGuessNumberGame(IGuessNumberGameSettings settings, IConsoleInputDataHelper consoleHelper)
    {
        _settings = settings;
        _consoleHelper = consoleHelper;
    }

    public void Play()
    {
        Console.WriteLine($"Играем! Загадано число от {_settings.From} до {_settings.To} у вас {_settings.Attempts} попыток");

        var rnd = new Random();
        var hiddenNumber = rnd.Next(_settings.From, _settings.To + 1);

        Console.WriteLine("Введите число");

        var guessedRight = false;
        for (var i = 0; i < _settings.Attempts; i++)
        {
            var userOption = _consoleHelper.GetIntFromConsole();

            if (userOption > hiddenNumber)
            {
                Console.Write("Меньше");
            }
            else if (userOption < hiddenNumber)
            {
                Console.Write("Больше");
            }
            else
            {
                guessedRight = true;
                break;
            }
            Console.WriteLine($", осталось {_settings.Attempts - i - 1} попыток");
        }

        Console.WriteLine(guessedRight ? "Угадал!" : $"Не угадал, число попыток закончилось. Было загадано число {hiddenNumber}. Повезет в другой раз");

        Console.WriteLine("Нажмите любу кнопку, чтобы вернуться в меню");
        Console.ReadLine();
    }
}