﻿using SOLID.Interfaces;

namespace SOLID.GuessNumberGame;

public class RandomSettingsGuessNumberGame : IGuessNumberGameSettings
{
    public RandomSettingsGuessNumberGame()
    {
        GenerateSettings();
    }

    public int From { get; protected set; }
    public int To { get; protected set; }
    public uint Attempts { get; protected set; }

    public virtual void ChangeSettings()
    {
        GenerateSettings();
    }

    private void GenerateSettings()
    {
        var rnd = new Random();
        From = rnd.Next(0, 25);
        To = rnd.Next(From + 1, 100);
        Attempts = (uint)rnd.Next(1, 11);
    }
}