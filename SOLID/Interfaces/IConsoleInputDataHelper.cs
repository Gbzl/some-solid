﻿namespace SOLID.Interfaces;

public interface IConsoleInputDataHelper
{
    int GetIntFromConsole();

    uint GetUintFromConsole();
}