﻿namespace SOLID.Interfaces;

public interface IGuessNumberGameSettings : ISettings
{
    int From { get; }
    int To { get; }
    uint Attempts { get; }
}