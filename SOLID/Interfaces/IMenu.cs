﻿namespace SOLID.Interfaces;

public interface IMenu
{
    void Start();
}