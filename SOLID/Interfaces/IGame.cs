﻿namespace SOLID.Interfaces;

public interface IGame
{
    void Play();
}