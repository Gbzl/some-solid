﻿namespace SOLID.Interfaces;

public interface ISettings
{
    void ChangeSettings();
}