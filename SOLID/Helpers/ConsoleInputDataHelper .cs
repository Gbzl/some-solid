﻿using SOLID.Interfaces;

namespace SOLID.Helpers;

public class ConsoleHelper : IConsoleInputDataHelper
{
    public int GetIntFromConsole()
    {
        do
        {
            var str = Console.ReadLine();
            if (str != null && int.TryParse(str, out var result))
                return result;
            
            Console.WriteLine("Введено некорректное значение, попробуйте еще раз");
        } while (true);
    }

    public uint GetUintFromConsole()
    {
        do
        {
            var str = Console.ReadLine();
            if (str != null && uint.TryParse(str, out var result))
                return result;

            Console.WriteLine("Введено некорректное значение, попробуйте еще раз");
        } while (true);
    }
}